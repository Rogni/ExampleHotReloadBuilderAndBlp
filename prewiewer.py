import argparse
import tempfile

import gi
gi.require_version('Peas', '2')
gi.require_version('Gio', '2.0')
gi.require_version('Gtk', '4.0')


from gi.repository import Gio, Peas, Gtk


class App(Gtk.Application):
    def __init__(self, filepath, plugin_dirs, *args, **kwargs):
        super().__init__(*args, application_id="prewiew.watcher", **kwargs)
        self.builder = None
        self.filepath = filepath
        self.root_window = None
        self.plugin_dirs = plugin_dirs

    def do_activate(self):
        self.engine = Peas.Engine.get_default()
        self.engine.enable_loader("python")
        for dir in self.plugin_dirs:
            self.engine.add_search_path(dir)
        
        self.engine.connect("load_plugin", self.load_plugin)
        self.engine.connect("unload_plugin", self.unload_plugin)

        self.engine.rescan_plugins()
        self.reload_plugins()
        self.reload_ui(self.filepath)

    def reload_plugins(self):
        self.engine.rescan_plugins()
        plugins = []
        for plugin in self.engine:
            plugins += [plugin.get_name()]
        self.engine.set_loaded_plugins(plugins)


    def reload_ui(self, builder_url):
        builder = Gtk.Builder()
        builder.add_from_file(builder_url)

        self.builder = builder
        root_object = None
        for obj in self.builder.get_objects():
            if issubclass(type(obj), Gtk.Window):
                root_object = obj
                break
            elif issubclass(type(obj), Gtk.Widget):
                if root_object is None:
                    root_object = obj
            else:
                pass
        if issubclass(type(root_object), Gtk.Window):
            self.root_window = root_object
            self.root_window.set_visible(True)
        elif root_object is not None:
            self.root_window = Gtk.Window()
            self.root_window.set_child(root_object)
            self.root_window.set_visible(True)

    def load_plugin(self, engine, info):
        pass

    def unload_plugin(self, engine, info):
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                        prog='BuilderPreview',
                        description='Preview xml builder')
    parser.add_argument('filename')
    parser.add_argument('--plugin_search_dirs', nargs='*', default=[])
    args = parser.parse_args()
    app = App(args.filename, args.plugin_search_dirs)
    app.hold()
    app.run()