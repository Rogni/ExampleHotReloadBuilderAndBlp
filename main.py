
from pathlib import Path

import argparse
import tempfile

import gi
gi.require_version('Peas', '2')
gi.require_version('Gio', '2.0')


from gi.repository import Gio, Peas

SOURCE_PATH = Path(__file__).resolve()
SOURCE_DIR = SOURCE_PATH.parent

class App(Gio.Application):
    def __init__(self, filepath, plugin_search_dirs, is_blueprint=False, *args, **kwargs):
        super().__init__(*args, application_id="prewiew.monitor", **kwargs)
        self.builder = None
        self.filepath = filepath
        self.tmp_file = tempfile.NamedTemporaryFile()
        self.is_blueprint = is_blueprint
        self.launcher = Gio.SubprocessLauncher()
        self.preview = None
        self.root_window = None
        self.plugin_search_dirs = plugin_search_dirs
        self.preview_func = self.preview_blueprint if self.is_blueprint else self.preview_xml_builder

    def do_activate(self):
        self.file = Gio.File.new_for_path(self.filepath)
        self.file_monitor = self.file.monitor_file(Gio.FileMonitorFlags.NONE)
        self.file_monitor.connect("changed", self.on_changed)
        self.preview_func()


    def preview_blueprint(self):
        blp_compiler = self.launcher.spawnv(["blueprint-compiler", "compile", "--output", self.tmp_file.name, self.filepath])
        blp_compiler.wait()
        self.reload_ui(self.tmp_file.name)
        

    def preview_xml_builder(self): 
        self.reload_ui(self.filepath)


    def reload_ui(self, builder_url):
        if self.preview is not None:
            self.preview.force_exit()
        self.preview = self.launcher.spawnv(["python", str(SOURCE_DIR / "prewiewer.py"), builder_url, "--plugin_search_dirs"] + self.plugin_search_dirs)

    def load_plugin(self, engine, info):
        print("load_plugin: ", info)

    def unload_plugin(self, engine, info):
        print("unload_plugin: ", info)

    def on_changed(self, file_monitor, file, other_file, event):
        if event == Gio.FileMonitorEvent.CHANGED:
            self.preview_func()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                        prog='PreviewMonitor',
                        description='Monitor xml builder or blueprint file and restert previewer')
    parser.add_argument('filename')
    parser.add_argument('-b', '--blueprint',
                    action='store_true')
    parser.add_argument('--plugin_search_dirs', nargs='*', default=[])
    args = parser.parse_args()
    
    app = App(args.filename, args.plugin_search_dirs, is_blueprint=args.blueprint)
    app.hold()
    app.run()